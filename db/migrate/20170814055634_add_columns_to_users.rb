class AddColumnsToUsers < ActiveRecord::Migration
  def change
    
    add_column :users, :name, :string
    add_column :users, :age, :integer
    add_column :users, :agree_on_term, :boolean
    add_column :users, :school, :string
    add_column :users, :gender, :integer
    
  end
end
