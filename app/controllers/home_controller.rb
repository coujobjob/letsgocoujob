class HomeController < ApplicationController
  
  before_action :authenticate_user!
  
  def index
    
    @coup_issued = current_user.get_up_voted Coupon
    
    if @coup_issued.size == 0
      @coupsize = 0
    else
      @coupsize = 1
    end
  end
  
  def issue_coupon
    issue_coupon = Coupon.new
    issue_coupon.user_id = current_user.id
    issue_coupon.save
    current_user.likes issue_coupon
    
    @coup_issued = current_user.get_up_voted Coupon
    
    if @coup_issued.size == 0
      @coupsize = 0
    else
      @coupsize = 1
    end
    
    respond_to do |format|
      format.html {render :nothing => true}
      format.js {}
    end
    
  end
  
  def use_coupon
    Coupon.find(params[:id]).downvote_from current_user
    
    @coup_issued = current_user.get_up_voted Coupon
    
    if @coup_issued.size == 0
      @coupsize = 0
    else
      @coupsize = 1
    end
    
    respond_to do |format|
      format.html {render :nothing => true}
      format.js {}
    end
    
  end
end
