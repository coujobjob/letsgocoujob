$(document).ready(function(){
// 모달 뒤로가기,
        
        if (window.location.hash && (window.location.hash == "#request")) {
            $(window.location.hash).modal('show');
        }
        
        if (window.location.hash && (window.location.hash == "#dropdownmenumenu")) {
            $("#dropdownmenumenu").css("display","block");
        }  
        
        $(document).on("click",'img[data-toggle="modal"]',function(){
            window.location.hash = $(this).attr('data-target');
        });
       
        function revertToOriginalURL() {
            var original = window.location.href.substr(0, window.location.href.indexOf('#'))
            history.pushState({}, document.title, original);
        }
    
        $(document).on('hidden.bs.modal','.modal', function () {
            revertToOriginalURL();
        });
        
        // 메뉴 드롭다운!
        $(document).on('click',".dropdownmenu_open", function (event) {
            event.preventDefault();
            $("#dropdownmenumenu").css("display","block");
            window.location.hash = $(this).attr('href');
        });
        
        $(document).on('click',"#close_dropdown", function (event) {
            revertToOriginalURL();
            $("#dropdownmenumenu").css("display","none");
            
        });
        
        $(window).on( 'hashchange', function(e) { 
            if(window.location.hash == "#request") {
                $(window.location.hash).modal('show');
            } else if (window.location.hash == "#dropmenumenu") {
                $("#dropdownmenumenu").css("display","block");
            } else {
                $("#dropdownmenumenu").css("display","none");
                $(".modal, .modal-backdrop.fade.in").modal('hide');
            }
        });    

});
